function Tetris(){
    const playfieldHeight = 20;
    const playfieldWidth = 10;

    const blockDefinition_I = [
        [0,0,0,0],
        [1,1,1,1]
    ];
    const blockDefinition_O = [
        [0,1,1,0],
        [0,1,1,0]
    ];
    const blockDefinition_T = [
        [0,1,0,0],
        [1,1,1,0]
    ];
    const blockDefinition_S = [
        [0,1,1,0],
        [1,1,0,0]
    ];
    const blockDefinition_Z = [
        [1,1,0,0],
        [0,1,1,0]
    ];
    const blockDefinition_J = [
        [1,0,0,0],
        [1,1,1,0]
    ];
    const blockDefinition_L = [
        [0,0,1,0],
        [1,1,1,0]
    ];

    // Reference for texture resources
    let blockResource_I = "images/I_Block.png";
    let blockResource_O = "images/O_Block.png";
    let blockResource_T = "images/T_Block.png";
    let blockResource_S = "images/S_Block.png";
    let blockResource_Z = "images/Z_Block.png";
    let blockResource_J = "images/J_Block.png";
    let blockResource_L = "images/L_Block.png";

    // Reference for sprites that have been loaded
    let blockTexture_I;
    let blockTexture_O;
    let blockTexture_T;
    let blockTexture_S;
    let blockTexture_Z;
    let blockTexture_J;
    let blockTexture_L;

    // Fill the browser's available area with out Pixi application
    let app = new PIXI.Application();
    app.renderer.view.style.position = "absolute";
    app.renderer.view.style.display = "block";
    app.renderer.autoDensity = true;
    app.renderer.resize(window.innerWidth, window.innerHeight);

    document.body.appendChild(app.view);

    // Load the sprites required for Tetris
    PIXI.Loader.shared.add([
            blockResource_I,
            blockResource_O,
            blockResource_T,
            blockResource_S,
            blockResource_Z,
            blockResource_J,
            blockResource_L
        ])
    .load(setup);

    function setup(){
        blockTexture_I = new PIXI.Sprite(PIXI.Loader.shared.resources[blockResource_I].texture);
        blockTexture_O = new PIXI.Sprite(PIXI.Loader.shared.resources[blockResource_I].texture);
        blockTexture_T = new PIXI.Sprite(PIXI.Loader.shared.resources[blockResource_I].texture);
        blockTexture_S = new PIXI.Sprite(PIXI.Loader.shared.resources[blockResource_I].texture);
        blockTexture_Z = new PIXI.Sprite(PIXI.Loader.shared.resources[blockResource_I].texture);
        blockTexture_J = new PIXI.Sprite(PIXI.Loader.shared.resources[blockResource_I].texture);
        blockTexture_L = new PIXI.Sprite(PIXI.Loader.shared.resources[blockResource_I].texture);
    }

    function StartGameLoop()
    {

    }

    function PauseGameLoop()
    {

    }

    function EndGameLoop()
    {

    }
}